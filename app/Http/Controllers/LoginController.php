<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function logar(Request $request){
        $dados = $request->all();
        $senha = bcrypt($dados['senha']);
        $usuario = \App\Models\Pessoa::where('matricula', $dados['matricula'])->first();
        if($usuario){
            if($usuario->senha==$senha){
                return view('logado')->compact(['usuario']);
             } else return view('erro_login');
        } else return view('erro_login');
    }
     
}


